import { prisma } from "@/app/lib/prisma"
import {NextResponse} from "next/server"


export async function POST(req:Request) {

    try {
        
        const {name, email,password} = await req.json()
        // const hash = await bcrypt.hash(password,12)

        const user  = await prisma.user.create({
            data : {
                name,email,password
            }
        })

    } catch (error) {

        if(error instanceof Error) {
            return NextResponse.json(
                {
                    status : "error", message:error.message
                },
                {
                    status : 500
                }
            )
            }
        
    }
    
}