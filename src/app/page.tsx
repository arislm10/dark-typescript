import Main from "./components/Main";
import About from "./components/About";
import Project from "./components/Project";
import Contact from "./components/Contact";
import { getServerSession } from "next-auth";
import { authOptions } from "@/app/lib/auth";

export default async function Home() {

  const session = await getServerSession(authOptions)
  
console.log('AAAA',session)
  return (
   <main>
       <Main />
       <About />
       <Project />
       <Contact />
   </main>
  )
}
