"use client";
import { Transition } from '@headlessui/react'
import { Fragment, useState,useEffect } from 'react'

export default function Example() {
  let [isShowing, setIsShowing] = useState<boolean>(true)

  function handleShow() {

    if(isShowing) {
      setIsShowing(false)
    }else {
      setIsShowing(true)
    }
    
  }



  return (
    <div className="flex flex-col items-center py-16">
      <div className="h-32 w-32">
        <Transition
          as={Fragment}
          show={isShowing}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          
          
            <div className="ml-auto w-80 h-full bg-surface-primary">
              <ul className="text-white text-right px-6">
                <li className="py-6">Home</li>
                <li className="py-6">About</li>
                <li className="py-6">Project</li>
                <li className="py-6">Contact</li>
              </ul>
            </div>
          
        </Transition>
      </div>

      <button
        onClick={() => {
          handleShow()
        
        }}
        className="backface-visibility-hidden mt-8 flex transform items-center rounded-full bg-black/20 px-3 py-2 text-sm font-medium text-white transition hover:scale-105 hover:bg-black/30 focus:outline-none active:bg-black/40"
      >
        

        <span className="ml-3">Click to transition</span>
      </button>
    </div>
  )
}
