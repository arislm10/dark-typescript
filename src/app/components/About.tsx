import React from "react";

export default function About() {
  return (
    <>
      <section>
        <div className="container w-full h-full bg-surface-primary rounded-2xl lg:px-16 lg:py-16">
          <div className="flex flex-col md:flex-row pt-12 pb-6 px-6 gap-8">
            <div className="flex md:pr-32 items-center justify-center w-full text-center">
              <img
                className="mx-auto max-w-full h-auto"
                width={600}
                src="/assets/designer-girl/computer.png"
                alt=""
              />
            </div>
            <div className="container w-full mx-auto">
              <div className="flex flex-row gap-6 pb-5">
                <img
                  src="/assets/icon-programing/JavaScript.png"
                  alt="JavaScript"
                />
                <img src="/assets/icon-programing/Js.png" alt="Js" />
                <img src="/assets/icon-programing/Python.png" alt="Python" />
                <img src="/assets/icon-programing/Tail.png" alt="Tail" />
                <img src="/assets/icon-programing/React.png" alt="React" />
              </div>
              <div>
                <p className="text-[#7DFFAF] font-Kalam">About me</p>
                <h2 className="font-Heebo text-2xl text-white">
                  I&rsquo;m a passionate software developer looking for my first
                  international opportunity
                </h2>
                <p className="font-Heebo text-base text-sc-abu py-6">
                  Beyond coding, I&apos;m a coffee enthusiast, a cat lover, and
                  a self-taught artist who enjoys spending my free time
                  doodling. I am currently seeking opportunities to bring my
                  skills and enthusiasm to a tech company in the United States
                  or Europe and am excited about the prospect of relocating to
                  pursue new challenges.
                </p>
              </div>
              <div>
                <button className="text-white font-Heebo bg-ungu px-8 py-4 rounded-lg text-base">
                  My resume
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
