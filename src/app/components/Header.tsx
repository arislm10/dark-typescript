"use client";
import Link from "next/link";
import { usePathname, useSearchParams } from "next/navigation";
import { Fragment, useState, useEffect } from "react";
import { Transition } from "@headlessui/react";
import {signIn,signOut} from "next-auth/react"
export default function Header() {
  let [isShowing, setIsShowing] = useState<boolean>(false);

  function handleShow() {
    if (isShowing) {
      setIsShowing(false);
    } else {
      setIsShowing(true);
    }
  }

  const pathname = usePathname();
  const searchParams = useSearchParams();

  useEffect(() => {
    const url = `${pathname}?${searchParams}`;
    console.log(url);
    // You can now use the current URL
    // ...
  }, [pathname, searchParams]);

  const NavLink = [
    {
      name: "Home",
      link: "/",
    },
    {
      name: "About",
      link: "/about",
    },
    {
      name: "Contact",
      link: "/contact",
    },
    {
      name: "Project",
      link: "/project",
    },
  ];

  const [hidden, setHidden] = useState<boolean>(true);

  useEffect(() => {
    // Lakukan sesuatu berdasarkan nilai boolean
  }, [hidden]);

  const handleHidden = () => {
    setHidden((prevStatus) => !prevStatus);
  };

  return (
    <>
      <section>
        <div className="container mx-auto">
          <div className="flex justify-between py-4 px-2">
            <Link href="/">
              <div className="text-white order-1 sm:order-1">
                <h1 className="font-Kalam text-[25px]">
                  Lina<span className="font-bold">Levi</span>
                </h1>
              </div>
            </Link>
            <div className="flex items-center justify-between order-2 md:order-3">
              <ul className="md:flex hidden gap-4">
                {NavLink.map(({ link, name }) => (
                  <Link
                    key={name}
                    href={link}
                    className={`${
                      pathname == link ? "text-white font-bold" : "text-sc-abu "
                    } font-Heebo text-sm`}
                  >
                    {name}
                  </Link>
                ))}
                <li className="font-Heebo text-sc-abu text-sm">
                    <Link href="/register">Register</Link>
                </li>
                <li className="font-Heebo text-sc-abu text-sm">
                    <Link href="/profile">Profile</Link>
                </li>
                <div className="flex gap-4 items-center justify-center ">
                  <button onClick={() => signIn()} className="text-white  font-Heebo bg-surface-secondary px-6 py-2 rounded-full  text-base ">
                    Sign In
                  </button>
                  <button onClick={() => signOut()} className="text-white  font-Heebo bg-surface-secondary px-6 py-2 rounded-full  text-base ">
                  Sign Out
                  </button>
                </div>
              </ul>
              <img
                className="items-center justify-center"
                src="/sun.svg"
                alt=""
              />
            </div>
            <div className="order-3 md:order-2">
              <img
                onClick={() => {
                  handleShow();
                  handleHidden();
                }}
                className={` md:hidden ${hidden ? "" : "hidden"}`}
                src="/List.svg"
                alt=""
              />
              <img
                onClick={() => {
                  handleShow();
                  handleHidden();
                }}
                width={30}
                height={30}
                className={` md:hidden lg:hidden ${hidden ? "hidden" : ""}`}
                src="/x-icon.svg"
                alt=""
              />
            </div>
          </div>
          <Transition
            as={Fragment}
            show={isShowing}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <div
              className={`absolute w-full md:hidden h-screen right-0 
            `}
            >
              <div className="ml-auto w-80 h-full bg-surface-primary">
                <ul className="text-white text-right px-6">
                  <li className="py-6">Home</li>
                  <li className="py-6">About</li>
                  <li className="py-6">Project</li>
                  <li className="py-6">Contact</li>
                  
                </ul>
              </div>
            </div>
          </Transition>
        </div>
      </section>
    </>
  );
}
