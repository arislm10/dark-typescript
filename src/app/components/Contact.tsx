import React from "react";

export default function Contact() {
  return (
    <>
      <section>
        <div className="container w-full h-full bg-surface-primary rounded-2xl px-16 py-16">
          <div className="flex flex-col md:flex-row mx-auto">
            <div className="flex flex-col mx-auto">
              <img
                className="mr-auto pb-10"
                src="/assets/designer-girl/cute.png"
                alt=""
              />

              <div className="">
                <p className="text-[#7DFFAF] font-Kalam text-base pb-4">
                  Contact
                </p>
                <h2 className="font-Heebo text-white text-2xl leading-[32px] pb-6">
                  Enjoyed my work? Let&rsquo;s work together
                </h2>
                <p className="text-sc-abu font-Heebo leading-6 pb-6">
                  I&rsquo;m always up for a chat. Pop me an email at{" "}
                  <span className="text-[#7DFFAF]">arispw28@gmail.com</span> or
                  give me a shout on social media.
                </p>
                <div className="flex gap-3">
                  <img src="/assets/logo-social-media/git.svg" alt="GitHub" />
                  <img src="/assets/logo-social-media/ig.svg" alt="Instagram" />
                  <img
                    src="/assets/logo-social-media/link.svg"
                    alt="LinkedIn"
                  />
                </div>
              </div>
            </div>
            <div className="md:w-1/2 mt-8">
              <div className="flex gap-3 flex-col">
                <div className="">
                  <input
                    className="bg-surface-background appearance-none border-2 border-surface-background rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-[#7DFFAF]"
                    id="inline-full-email"
                    placeholder="Name"
                    type="text"
                  />
                </div>
                <div className="">
                  <input
                    className="bg-surface-background appearance-none border-2 border-surface-background rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-[#7DFFAF]"
                    id="inline-full-email"
                    placeholder="E-Mail"
                    type="email"
                  />
                </div>
                <div className="">
                  <textarea
                    className="bg-surface-background appearance-none border-2 border-surface-background rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-[#7DFFAF]"
                    id="inline-full-email"
                    placeholder="Your message"
                  />
                </div>
              </div>
              <div className="mt-6">
                <button className="flex gap-3 text-white font-Heebo bg-ungu px-8 py-4 rounded text-base">
                  Send a message
                  <img src="/arrow.svg" alt="Arrow" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
