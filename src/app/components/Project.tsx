import React from "react";
import Link from 'next/link'

export default function Project() {
  return (
    <>
      <section>
        <div className="container mx-auto">
          <div className="flex flex-col pt-12 pb-6 px-6 gap-8">
            <div className="text-center">
              <p className="text-[#7DFFAF] leading-4 text-base font-Kalam">
                Projects
              </p>
            </div>

            <h2 className="text-2xl text-white font-Heebo text-center">
              Take a look at my highlighted projects
            </h2>

            <div>
              <div className="flex gap-8 flex-col md:flex-row lg:flex-row">
                <div className="py-6 px-6  bg-surface-primary rounded-lg w-full lg:w-2/6">
                  <img className="" src="/assets/project/1.png" alt="" />

                  <div className="flex justify-between">
                    <div className="">
                      <p className="text-sc-abu text-sm pt-4">Jul-Dec 2022</p>
                    </div>
                    <div className="flex gap-4">
                      <img
                        className="pt-4"
                        src="/assets/icon-programing/JavaScript.png"
                        alt=""
                      />
                      <img
                        className="pt-4"
                        src="/assets/icon-programing/Js.png"
                        alt=""
                      />
                    </div>
                  </div>
                  <div className="leading-4">
                  <Link href="/project">
                    <p className="text-xl  font-Heebo text-white pt-4">
                      DevLinks
                    </p>
                    <span className="text-base  text-white font-Heebo text-ellipsis">
                      A link agragator for social media.
                    </span>
                    </Link>
                  </div>
                </div>
                <div className="py-6 px-6  bg-surface-primary rounded-lg w-full lg:w-2/6">
                  <img className="" src="/assets/project/1.png" alt="" />
                  <div className="flex justify-between">
                    <div className="">
                      <p className="text-sc-abu text-sm pt-4">Jul-Dec 2022</p>
                    </div>
                    <div className="flex gap-4">
                      <img
                        className="pt-4"
                        src="/assets/icon-programing/JavaScript.png"
                        alt=""
                      />
                      <img
                        className="pt-4"
                        src="/assets/icon-programing/Js.png"
                        alt=""
                      />
                    </div>
                  </div>
                  <div className="leading-4">
                  <Link href="/project">
                    <p className="text-xl  font-Heebo text-white pt-4">
                      DevLinks
                    </p>
                    <span className="text-base  text-white font-Heebo text-ellipsis">
                      A link agragator for social media.
                    </span>
                    </Link>
                  </div>
                </div>
                <div className="py-6 px-6  bg-surface-primary rounded-lg w-full lg:w-2/6">
                  <img className="" src="/assets/project/1.png" alt="" />
                  <div className="flex justify-between">
                    <div className="">
                      <p className="text-sc-abu text-sm pt-4">Jul-Dec 2022</p>
                    </div>
                    <div className="flex gap-4">
                      <img
                        className="pt-4"
                        src="/assets/icon-programing/JavaScript.png"
                        alt=""
                      />
                      <img
                        className="pt-4"
                        src="/assets/icon-programing/Js.png"
                        alt=""
                      />
                    </div>
                  </div>
                  <div className="leading-4">
                    <Link href="/project">
                    <p className="text-xl  font-Heebo text-white pt-4">
                      DevLinks
                    </p>
                    <span className="text-base  text-white font-Heebo text-ellipsis">
                      A link agragator for social media.
                    </span>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="">
              <button className="flex font-Heebo gap-2 mx-auto justify-center items-center py-4 px-6 bg-surface-secondary rounded-2xl text-white">
                See all
                <img src="/arrow.svg" alt="" />
              </button>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
