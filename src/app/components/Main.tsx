import React from "react";

export default function Main() {
  return (
    <>
      <section>
        <div className="container w-full h-full   ">
          <div className="flex flex-col md:flex-row lg:flex-row lg:mx-28 lg:my-24 pb-6 px-6 gap-12">
            <div className="order-1 lg:order-2 w-full h-full ">
              <img
                className="mx-auto"
                src="/assets/designer-girl/bro.png"
                alt=""
              />
            </div>

            <div className="leading-tight flex flex-col gap-4 order-2 lg:order-1">
              <h1 className="text-[40px] pb-6 bg-gradient-to-r from-[#9955E8] to-[#7BFFAF] font-Kalam text-transparent bg-clip-text">
                Hi, I’m Aris Munandar
              </h1>
              <h2 className="text-white font-bold font-Heebo text-[24px]">
                Full-stack developer and innovation enthusiast
              </h2>
              <p className="text-sc-abu font-Heebo text-base leading-6">
                Over 4 years of experience in the tech industry. I specialize in
                building innovative web and mobile applications using
                technologies such as React, React Native, and Node.js.
              </p>
              <div className="lg:mr-auto pt-6 pb-12">
              <div className="flex gap-4 items-center justify-center ">
                <button className="text-white  font-Heebo bg-surface-secondary px-8 py-4 rounded  text-base ">
                  My resume
                </button>
                <button className="text-white   font-Heebo bg-ungu px-8 py-4 rounded  text-base ">
                  Get in touch
                </button>
              </div>
            </div>
            </div>

           
          </div>
        </div>
      </section>
    </>
  );
}
