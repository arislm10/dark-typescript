import React from "react";
import RootLayout from "../../layout";

export default function page() {
  return (
    <>
      <div className="container mx-auto">
        <div className="flex flex-col md:flex-row mx-8 my-8 gap-6">
          <div className="md:w-2/3">
            <img className="md:rounded-2xl" src="/assets/project/1.png" alt="" />
            <div className="flex justify-between">
              <div className="">
                <p className="text-sc-abu text-sm pt-4">Jul-Dec 2022</p>
              </div>
              <div className="flex gap-4">
                <img
                  className="pt-4"
                  src="/assets/icon-programing/JavaScript.png"
                  alt=""
                />
                <img
                  className="pt-4"
                  src="/assets/icon-programing/Js.png"
                  alt=""
                />
              </div>
            </div>
            <div className="leading-4">

              <div>
                <h1 className="text-pr-putih text-3xl py-6">Feedback Widget</h1>
                <div className="text-pr-putih text-lg leading-5 bg-surface-primary px-6 py-6 md:px-8 md:py-8 rounded-2xl">

                  <p className="mb-4">
                    My role: Full-stack developer
                  </p>

                  <p className="mb-4">
                    Team: Marcus Souza (PM), Ilana Mallak (UX/UI Designer)
                  </p>

                  <p className="mb-8">
                    We improved our CSAT from 4.4 to 4.8 after analyzing customer feedback provided on our product through this widget.
                  </p>

                  <p className="mb-8">
                    I worked as the main developer of this feature, implementing the front-end using Tailwind and a Data Viz dashboard using Python to follow-up customer feedback and improve data analysis.
                  </p>

                  <p>
                    The main challenge was to create a flexible structure that could be used as an API across all our company web applications.
                  </p>

                </div>
              </div>
            </div>
          </div>
          <div className="bg-surface-primary h-full px-8 py-8 rounded-2xl md:w-1/3">
            <div className="pb-6">
              <h2 className=" text-pr-putih text-xl font-Heebo">Take a look at this project</h2>
            </div>
            <div className="flex flex-col gap-4 items-center justify-center ">
              <button className="flex text-white w-full  font-Heebo bg-ungu px-6 py-4 rounded  text-base justify-between items-center ">
                <img src="/globe.png" alt="" />
                Live Demo
                <img src="/arrow.svg" alt="" />
              </button>
              <button className="flex gap-3 text-white w-full  font-Heebo bg-surface-secondary px-8 py-4 rounded  text-base justify-between items-center ">
                <img src="/github.png" alt="" />
                Code
                <img src="/arrow.svg" alt="" />
              </button>
            </div>
          </div>
        </div>

      </div>

    </>
  );
}   
