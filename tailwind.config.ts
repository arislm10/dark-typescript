import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      
      colors: {
        "ungu" : '#8A42DB',
        "hijau" : '#7DFFAF',
        "surface-background" : '#171023',
        "surface-primary" : '#2C243B',
        "surface-secondary" : '#413A4F',
        "surface-tertiary" : '##4E4563',
        "pr-putih" :  '#F5F6F6',
        "sc-abu" : '#CDD0D4'

      },

      fontFamily : {
        "Kalam" : ['Kalam'],
        "Heebo" : ['Heebo']
      }     
    },
  },
  plugins: [],
}
export default config
